#!/bin/bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

mkdir -p $HOME/.local/share/icons &>/dev/null

cp -f $SCRIPT_DIR/icons/kodi-256.png $HOME/.local/share/icons/

cp -f $SCRIPT_DIR/resources/run.sh $HOME/.local/bin/kodi_podman_run.sh

cp -f $SCRIPT_DIR/resources/changeres*.sh $HOME/.local/bin/

chmod 700 $HOME/.local/bin/kodi_podman_run.sh $HOME/.local/bin/changeres*.sh

cat << EOF > $HOME/.local/share/applications/kodi_podman.desktop
[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=Kodi Podman
Comment=Kodi Media Player
Icon=$HOME/.local/share/icons/kodi-256.png
Exec=$HOME/.local/bin/kodi_podman_run.sh
Terminal=false
Categories=Multimedia;Network;Application
EOF

chmod 600 $HOME/.local/share/icons/kodi-256.png $HOME/.local/share/applications/kodi_podman.desktop
