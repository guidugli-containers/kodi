#!/bin/bash

set -xeu -o pipefail

function get_serial() {
  serial="$(gdbus call --session --dest org.gnome.Mutter.DisplayConfig \
  --object-path /org/gnome/Mutter/DisplayConfig \
  --method org.gnome.Mutter.DisplayConfig.GetResources | awk '{print $2}' | tr -d ',')"
  echo $serial
}

serial=$(get_serial)
gdbus call --session --dest org.gnome.Mutter.DisplayConfig \
  --object-path /org/gnome/Mutter/DisplayConfig \
  --method org.gnome.Mutter.DisplayConfig.ApplyMonitorsConfig \
  $serial 1 '[(1920, 0, 1, 0, false, [('\''HDMI-2'\'', '\''3840x2160@30'\'', [] )] ),  (0, 0, 1, 0, true, [('\''DP-1'\'', '\''1920x1080@60'\'', [] )] )]' '[]'

