#!/bin/bash

ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
CONTAINER_NAME="kodi"
CONTAINER_IMAGE="docker.io/guidugli/kodi:latest"

## PARAMETERS ##

# Dont really dbus to have the browser working
ENABLE_DBUS=0

# Set timezone. By default use same as host system
TIMEZONE="$(timedatectl show --value -p Timezone)"

# Path where Firefox data will be stored
# Comment the line below to store all data inside container but
# all data will be lost after container execution
DATA_PATH=${HOME}/container-data/kodi

# If DATA_PATH is commented, a new profile will be created and used,
# then deleted at the end of the session. Instead, if DATA_PATH is
# specified and DATA_PATH_DISCARD_ON_EXIT is set to 1 (one), the
# container will use the path specified by DATA_PATH but all changes
# will be discarded on exit
DATA_PATH_DISCARD_ON_EXIT=0

# Sometimes sharing hardware like webcams may need share udev
SHARE_UDEV_DATA=0

# Specify script to change resolution when starting Kodi. For example,
# when starting Kodi chnage the resolution of the monitor from 4k to 1080p
CHANGE_RESOLUTION_START_SCRIPT=$ABSOLUTE_PATH/changeres1920x1080.sh

# Specify script to change resolution when kodi is stopped. Use this
# to restore the resolution set by the other script.
CHANGE_RESOLUTION_STOP_SCRIPT=$ABSOLUTE_PATH/changeres3840x2160.sh

######################################################

# Get user information
USER_UID=$(id -u)
USER_GID=$(id -g)
USER_NAME=$(id -un)

# Easy to move to docker or using sudo
PROG='podman'

# mandatory parameters
MANDATORY="--userns=keep-id --security-opt label=disable"

# Devices
DEVICES="--device /dev/snd"
DEVICES+=" --device /dev/dri"

# Environment variables
ENV_VARS="-e PULSE_SERVER=unix:${XDG_RUNTIME_DIR}/pulse/native"
ENV_VARS+=" -e DISPLAY=$DISPLAY"
ENV_VARS+=" -e XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR"
#ENV_VARS+=" -e WAYLAND_DISPLAY=$WAYLAND_DISPLAY"
ENV_VARS+=" -e USER_UID=$USER_UID"
ENV_VARS+=" -e USER_GID=$USER_GID"
ENV_VARS+=" -e USER_NAME=$USER_NAME"

if [ -n "${XAUTH}" ]; then
  ENV_VARS+=" -e XAUTHORITY=${XAUTH}"
fi

if [ -n "$TIMEZONE" ]; then
  ENV_VARS+=" -e TZ=$TIMEZONE"
fi

if [ $ENABLE_DBUS -eq 1 ]; then
  ENV_VARS+=" -e DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS"
fi

# Networking
NETWORKING="-p 8123:8123/tcp -p 9777:9777/udp"

# Volumes to be mapped
VOLUMES="-v ${XDG_RUNTIME_DIR}/pulse/native:${XDG_RUNTIME_DIR}/pulse/native"
VOLUMES+=" -v /tmp/.X11-unix:/tmp/.X11-unix"
VOLUMES+=" -v /etc/machine-id:/etc/machine-id:ro"
VOLUMES+=" -v /dev/shm:/dev/shm"
#VOLUMES+=" -v ${XDG_RUNTIME_DIR}/${WAYLAND_DISPLAY}:${XDG_RUNTIME_DIR}/${WAYLAND_DISPLAY}"

if [ -n "${DATA_PATH}" ]; then
  VFLAG=':Z'
  if [ "$DATA_PATH_DISCARD_ON_EXIT" -eq 1 ]; then
    VFLAG=":O"
  fi

  mkdir -p ${DATA_PATH} 2>/dev/null
  VOLUMES+=" -v ${DATA_PATH}:/home/$USER_NAME/.kodi${VFLAG}"
  VFLAG=
fi

if [ $ENABLE_DBUS -eq 1 ]; then
  VOLUMES+=" -v ${XDG_RUNTIME_DIR}/bus:${XDG_RUNTIME_DIR}/bus"
  VOLUMES+=" -v /run/dbus/system_bus_socket:/run/dbus/system_bus_socket"
fi

if [ $SHARE_UDEV_DATA -eq 1 ]; then
  VOLUMES+=" -v /run/udev/data:/run/udev/data:ro"
fi

### Configure monitor ###

# Store the time to blank screen
DELAY=`gsettings get org.gnome.desktop.session idle-delay | cut -d' ' -f2`

# Get secondary monitor name
#SECMON=`xrandr --listactivemonitors | grep "1:" | awk '{ print $4 }'`

# Change Kodi configuration file to adjust to current secondary monitor name
# For some reason the name change after the session enters screensaver/lock
#sed -i -E "s|<setting id=\"videoscreen.monitor\">.+</setting>|<setting id=\"videoscreen.monitor\">$SECMON</setting>|g" $DATA_PATH/userdata/guisettings.xml

### START ###

gsettings set org.gnome.desktop.session idle-delay 0

if [ -n "$CHANGE_RESOLUTION_START_SCRIPT" ]; then
  $CHANGE_RESOLUTION_START_SCRIPT
fi

$PROG ps -a | grep "$CONTAINER_NAME" &>/dev/null
if [ $? -eq 0 ]; then
    $PROG start "$CONTAINER_NAME"
else
    $PROG run -it $MANDATORY $DEVICES $ENV_VARS $NETWORKING $VOLUMES -u root --group-add users --name $CONTAINER_NAME $CONTAINER_IMAGE
fi

while true
do
    $PROG ps -a -f status=running | grep $CONTAINER_NAME &>/dev/null
    if [ $? -ne 0 ]; then
        break
    fi
    sleep 10
done

gsettings set org.gnome.desktop.session idle-delay $DELAY

if [ -n "$CHANGE_RESOLUTION_STOP_SCRIPT" ]; then
  $CHANGE_RESOLUTION_STOP_SCRIPT
fi

